# Huntsworth Health Footer
Huntsworth Health requires all Huntsworth Health companies to utilize the same footer design on their sites. This repository includes all files you will need to install the Huntsworth Health footer on your site, along with detailed instructions below.
### Initial Setup
#### HTML
Open the __hh-footer.html__ file, and copy / paste the HTML code into your project. This should be the last HTML element on your page.

```html
<footer class="hh-footer">
    <a href="http://hhealth.com" target="_blank" title="Huntsworth Health">
        <img src="/assets/images/icons/hh-logo-light.png" alt="" class="hh-logo hh-logo-light">
        <img src="/assets/images/icons/hh-logo-dark.png" alt="" class="hh-logo hh-logo-dark">
    </a>
    <div class="hh-footer-inner">
        <div class="hh-footer-tag hh-footer-tag-small">
            <p><a href="http://hhealth.com" target="_blank">a Huntsworth Health agency</a></p>
        </div>
        <div class="hh-footer-copyright">
            <p>&copy; Copyright 2016. All rights reserved. <!--<a href="#">Terms and privacy.</a>--></p>
        </div>
        <div class="hh-footer-tag hh-footer-tag-large">
            <p><a href="http://hhealth.com" target="_blank">a Huntsworth Health agency</a></p>
        </div>
    </div>
</footer>
```

#### Images
Inside the __img__ folder in this repo, copy the __hh-logo-dark.png__ and __hh-logo-light.png__ image files somewhere in your project’s assets folder. 
Remember to update the IMG file path in the HTML to match your new relative file path, replacing __/assets/images/icons/__ in both places below.

```html
<img src="/assets/images/icons/hh-logo-light.png" alt="" class="hh-logo hh-logo-light">
<img src="/assets/images/icons/hh-logo-dark.png" alt="" class="hh-logo hh-logo-dark">
```

#### CSS
You can find all CSS files inside the __css__ folder in this repo. If your project uses LESS or SCSS, you can include the corresponding stylesheet file type in your project.
* hh-footer.less
* hh-footer.scss

Otherwise, you can open the __hh-footer.css__ file, and copy / paste the CSS code into one of your existing CSS files.

### Color Themes
The footer comes in two color themes. The default is the “dark” theme, where the background is dark gray, and the logo and text are white. There is also a “light” theme, where the background is white, and the logo and text are dark gray.
To enable the light theme, add the class __hh-footer-light__ to the footer tag:

```html
<footer class=”hh-footer hh-footer-light”>
```

### Copyright
```html
<div class="hh-footer-copyright">
      <p>&copy; Copyright 2016. All rights reserved. <!--<a href="#">Terms and privacy.</a>--></p>
</div>
```

The copyright section currently contains the text “2016” to denote the year. You will want to make sure this stays updated. If your site is using a templating language like PHP or Twig, you can keep this updated by using a date() function in place of the “2016” text.

*Twig __date()__ function example*
```twig
<p>&copy; Copyright {{ “now”|date(“Y”) }}. All rights reserved. <!--<a href="#">Terms and privacy.</a>--></p>
```

#### Terms and Privacy
The copyright section contains a link with the text “Terms and privacy.” This is currently commented out. If your site does not require terms and privacy information, you can delete this code from the HTML:

```html
<!--<a href="#">Terms and privacy.</a>-->
```

Otherwise, you will want to uncomment this line by removing the __<!- -__ and __- ->__ code surrounding the anchor tag, like so:

```html
<a href="#">Terms and privacy.</a>
```

From here, we recommend either changing the HREF attribute value to link to a page on your site, or adding an ID or class that can trigger a modal when clicking the “Terms and privacy” text.

*HREF example*
```html
<a href=”YOUR-PAGE-LINK.html”>Terms and privacy.</a>
```

*Modal class example*
```html
<a href=”#” class=”YOUR-MODAL-LINK”>Terms and privacy.</a>
```

Replace the text in all-caps above with your own page name or class/ID, respectively.

### Tagline
```html
<p><a href="http://hhealth.com" target="_blank">a Huntsworth Health agency</a></p>
```
In order to maximize browser compatibility, this footer requires that the tagline “a Huntsworth Health agency” be included in the HTML twice. This is because the order of the content switches in different viewports—on small and/or mobile screens, the tagline appears before the copyright information.


The tagline HTML displayed on small screens uses the parent class __hh-footer-tag-small__, while the tagline HTML displayed on large screens uses the parent class __hh-footer-tag-large__.

*Small screen tagline, __hh-footer-tag-small__*
```html
<div class="hh-footer-tag hh-footer-tag-small">
    <p><a href="http://hhealth.com" target="_blank">a Huntsworth Health agency</a></p>
</div>
```

*Large screen tagline, __hh-footer-tag-large__*
```html
<div class="hh-footer-tag hh-footer-tag-large">
    <p><a href="http://hhealth.com" target="_blank">a Huntsworth Health agency</a></p>
</div>
```

If you want to update the tagline text, or make any other changes, make sure to update both taglines in the HTML accordingly.